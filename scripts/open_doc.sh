#!/usr/bin/env sh

set -eu

(
  gio="gio open"
  cd "$(dirname "$0")/../"
  if ! command -v gio > /dev/null; then
      gio="xdg-open";
  fi
  $gio ./very_hungry_penguins_common_api.docdir/index.html;
)
